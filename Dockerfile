# Version : 0.3
FROM nginx
MAINTAINER Beny Ibrani "beny@berbagitulisan.com"
RUN apt-get update; apt-get install -y nginx openvpn
RUN apt install openvpn -y
RUN /etc/init.d/nginx start
RUN echo 'Hi, I am in your container' > /var/www/html/index.html
EXPOSE 80
